Contributing
============
Please note that this project is released with a
[Contributor Code of Conduct][]. By participating in this project you agree to
abide by its terms.

Take care to maintain the existing coding style.  Add unit tests for any new or
changed functionality.  Lint and test your code using [Jest][] via `yarn test`.

[Contributor Code of Conduct]: http://www.chameleoid.com/conduct
[Jest]: https://jestjs.io/
