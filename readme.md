Telepathy
=========
A sync-less "telepathic" password manager that generates repeatable
site-specific passwords across any of your devices.

## How does it work?
At the core, it uses HASH(secret + user + domain) -> base-convert(62/94)

## There are several repositories within the Telepathy group
- [About][] - This repository
- [Lib][] - Library providing core functionality


[About]: https://gitlab.com/chameleoid/telepathy/about
[Lib]: https://gitlab.com/chameleoid/telepathy/lib
